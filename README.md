# ProjetoAPI

<div align="center">
      <h2>Menu</h2>
      <p>
            <a href="#introducao"><span>:small_blue_diamond:</span>Introdução </a>
            <a href="#comousar"><span>:small_blue_diamond:</span>Como usar</a>
            <a href="#arquitetura"><span>:small_blue_diamond:</span>Arquitetura</a>
            <a href="#backlog"><span>:small_blue_diamond:</span>Backlog</a>
            <a href="#bd"><span>:small_blue_diamond:</span>Banco de Dados</a>
            <a href="#equipe"><span>:small_blue_diamond:</span>Equipe</a>
      </p>
</div>

<a name="introducao"></a>
## :scroll: Introdução

Organizado pela Fatec de São José dos Campos em conjunto com as melhores empresas da região do Vale do Paraíba, o API é uma iniciativa que envolve os alunos da fatec em projetos reais. 

Para cada semestre do curso de Banco de Dados é selecionado uma empresa que apresenta um problema para ser desenvolvido pelos alunos, e para a turma do 4º semestre de Banco de Dados foi apresentado o seguinte problema: Desenvolver uma aplicação que resolva o problema de "match" entre candidatos e vagas, onde se possa alcançar, de maneira escalável e ágil a contratação dos melhores candidatos para as vagas apresentadas, através de uma gestão simples e eficiente.

### Visão do Projeto

O projeto Nemo visa ser uma solução simples, versátil, escalável e open source para pessoas e empresas que precisam de um sistema escalável, simples e versátil para fazer a gestão dos currículos de candidatos relacionando eles às vagas disponíveis pela empresa.

### Apresentação da Evolução do Projeto
:white_large_square: Sprint 1  | :white_large_square: Sprint 2 | :white_large_square: Sprint 3 | :white_large_square:  Sprint 4  
--------- |--------- |--------- |--------- |
[Aguarde]() |[Aguarde]() |[Aguarde]() |[Aguarde]() |

### Planejamento

Para fazer o planejamento foi utilizado a metodologia de "Design Thinking". Segundo o wikipedia, Design Thinking é o conjunto de ideias e insights para abordar problemas, relacionados a futuras aquisições de informações, análise de conhecimento e propostas de soluções.

:arrows_counterclockwise:O design thinking consiste em 5 partes ciclicas: empatia, definição, ideação, prototipo, teste.

- A primeira etapa (empatia) do primeiro ciclo foi realizada junto ao cliente.
- A etapa de definição é realizada durante as plannings do projeto.
- A etapa de ideação e prototipo são realizadas durante a sprint e 
- A etapa de teste é realizada sempre que um prototipo vira funcionalidade através do fluxo de CI (continuous integration) e CD (continuous deploy/delivery) do projeto.

### Cronograma

- [ ] xx/xx/2021 até xx/xx/2021 - Kick Off do Projeto
- [ ] xx/xx/2021 até xx/xx/2021 - Sprint 1
- [ ] xx/xx/2021 até xx/xx/2021 - Sprint 2
- [ ] xx/xx/2021 até xx/xx/2021 - Sprint 3
- [ ] xx/xx/2021 até xx/xx/2021 - Sprint 4
- [ ] xx/xx/2021 até xx/xx/2021 - Sprint Apresentação Final
- [ ] xx/xx/2021 até xx/xx/2021 - Sprint Feira de Soluções

### Tecnologias Utilizadas

<div align="center">
      <img src="#">
</div>

## :capital_abcd: Como usar

### No seu ambiente

- a -  Agora acesse através do POSTMAN ou do INSOMNIA a rota: "localhost:8081/nemo/v1/authentication" para se autenticar,

- n - Copie o token e agora insira ele no header da requisição para fazer requisições para as outras rotas da aplicação. 

### Na cloud do projeto

- Momentaneamente desativado

<a name="arquitetura"></a>
## :bookmark_tabs: Arquitetura do Projeto

### Arquitetura do MVP:

![MVP_Architecture](#)

----------------------------------------------------------------------------------------------

### Design da aplicação:

![MVP_System_Design](#)

<a name="backlog"></a>
## :memo: Backlog

### Requisitos Funcionais (Story Cards)

Na descrição dos story cards, entende-se como usuário o recrutador ou responsável por analisar os currículos e como candidato a pessoa que deseja se candidatar à uma vaga.

#### :white_medium_square: Sprint 1
<strong>#1</strong> - 



<strong>#2</strong> - 

<strong>#3</strong> -

<strong>#4</strong> - 

#### :white_medium_square: Sprint 2


<strong>#5</strong> - 

<strong>#6</strong> - 

####  :white_medium_square: Sprint 3

<strong>#7</strong> - 

<strong>#8</strong> - 

#### :white_medium_square: Sprint 4

<strong>#9</strong> - 

<strong>#10</strong> - 

### :white_square_button: Requisitos não Funcionais

- Documentação completa e clara
- Relatórios de desempenho
- Segurança
- Escalabilidade
- Performance
- Testes

<a name="bd"></a>
## :floppy_disk: 5. Diagrama do Banco de Dados

 ![Diagrama do Banco de Dados](/uploads/38e76438de710b0167f9ee21c60b6734/API.png)


## :muscle: Equipe

| André Lars | Daniel Delgado | Felipe Braga | Giovanni Guidace | Jéssica Isri  |
|---|---|---|---|---|
| [linkedIn](https://www.linkedin.com/in/andre-lars-da-cunha/) | [linkedIn](https://www.linkedin.com/in/daniel-delgado-274096194/) | [linkedIn](https://www.linkedin.com/in/felipegbraga/) |  [linkedIn](https://www.linkedin.com/in/giovanni-guidace-61982812a/) | [linkedIn](https://www.linkedin.com/in/jessica-dias1/) |
| <img src="/uploads/927687275b2adc4516f5335428878ed4/lars.jpeg" width="100px"> | <img src="/uploads/57f5ec24639c1e4790c370d5d7dda0b9/daniel.jpg" width="100px"> | <img src="/uploads/b0902b683fc9a6206ddce5a229b3aa68/felipe.jpg" width="100px"> | <img src="/uploads/d009a05b46763fd149d5f9efca99ffb7/giovanni.jpeg" width="100px"> | <img src="/uploads/47725d181672a229b7c5b720fc3aed46/jessica.jpeg" width="100px"> |
